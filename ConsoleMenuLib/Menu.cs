﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleMenuLib
{
    public class Menu
    {
        public static char? RunMenu(Dictionary<char, string> menu, bool showWarning = true)
        {
            var sb = new StringBuilder();
            foreach(var kvp in menu)
            {
                var c = char.ToUpper(kvp.Key);
                var txt = kvp.Value;

                sb.AppendLine($"{c}: {txt}");
            }

            sb.AppendLine();
            sb.Append("Choose  > ");

            Console.Write(sb.ToString());
            while (true)
            {
                var pressedKey = Console.ReadKey(true);
                if (pressedKey.Key == ConsoleKey.Escape) return null;
                // Now check to see which key was pressed
                if(showWarning)
                    Console.WriteLine(char.ToUpper(pressedKey.KeyChar));
                if (menu.ContainsKey(pressedKey.KeyChar))
                    return pressedKey.KeyChar;
                if (menu.ContainsKey(char.ToUpper(pressedKey.KeyChar)))
                    return char.ToUpper(pressedKey.KeyChar);
                // Invalid choice.
                if(showWarning)
                    Console.Write("Invalid choice.  Please try again > ");
            }
        }
    }
}
