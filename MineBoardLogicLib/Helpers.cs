﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MineBoardLogicLib
{
    public static class Helpers
    {
        public static void IterateBoard(Action<int, int> boardAction, int size = 8)
        {
            for(var x = 0; x < size; x++)
            for (var y = 0; y < size; y++)
                boardAction(x, y);
        }
    }
}
