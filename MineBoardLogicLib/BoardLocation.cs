﻿using System;

namespace MineBoardLogicLib
{
    public struct BoardLocation
    {
        private int x;
        private int y;

        public int X
        {
            get => x;
            set
            {
                if (value < 0 | value > 7) throw new ArgumentOutOfRangeException("X must be between 0 and 7");
                x = value;
            }
        }

        public int Y
        {
            get => y; 
            set
            {
                if (value < 0 | value > 7) throw new ArgumentOutOfRangeException("Y must be between 0 and 7");
                y = value;
            }
        }

        public override string ToString()
        {
            // A1 is bottom left, hence it is X = 0, y = 7
            // H8 is top right, hence it is X = 7, y = 0

            char xChar = (char)('A' + X);
            char yChar = (char)('1' + (7 - Y));

            return $"{xChar}{yChar}";
        }

        public BoardLocation Clone() => new BoardLocation { X = X, Y = Y };
    }
}
