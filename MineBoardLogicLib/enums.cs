﻿using System;

namespace MineBoardLogicLib
{
    public enum MoveResult
    {
        OutOfBounds,
        HitMine,
        OK,
        ReachedTarget
    }


    [Flags] // Might want to do diagonal at some point...
    public enum MoveType
    {
        Up = 1,
        Down = 2,
        Left = 4,
        Right = 8
    }


    public enum BoardLocationState
    {
        Empty,
        Player,
        Mine,
        Unknown,
        PreviousPlayer
    }

    public enum GameDifficulty
    {
        Easy = 0,
        Medium = 1,
        Hard = 2,
        Insane = 3
    }
}
