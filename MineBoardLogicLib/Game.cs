﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MineBoardLogicLib
{
    public class Game
    {
        internal Board Board { get; private set; } = new Board();

        public int Score { get; private set; }
        public bool GameOver { get; private set; }
        public int Lives { get; private set; }
        private readonly List<BoardLocation> _playerPath = new List<BoardLocation>();

        public string PlayerLocationAsChess => Board.PlayerLocation.ToString();
        internal void SetTestLives(int lives) => Lives = lives;

        public void StartGame(GameDifficulty difficulty)
        {
            Board = new Board();
            var numMines = 0;
            switch (difficulty)
            {
                case GameDifficulty.Easy: numMines = 8; Lives = 5; break;
                case GameDifficulty.Medium: numMines = 12; Lives = 5; break;
                case GameDifficulty.Hard: numMines = 16; Lives = 4; break;
                case GameDifficulty.Insane: numMines = 24; Lives = 3; break;
            }

            Board.Init(numMines);
            _playerPath.Add(Board.PlayerLocation);
        }

        public MoveResult MakeMove(MoveType move)
        {
            var newLocation = Board.PlayerLocation.Clone();
            try
            {
                switch (move)
                {
                    case MoveType.Up: newLocation.Y -= 1; break;
                    case MoveType.Down: newLocation.Y += 1; break;
                    case MoveType.Left: newLocation.X -= 1; break;
                    case MoveType.Right: newLocation.X += 1; break;
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                return MoveResult.OutOfBounds;
            }

            var moveResult = Board.DoMove(newLocation);
            if (moveResult != MoveResult.OutOfBounds)
            {
                if (moveResult == MoveResult.HitMine)
                {
                    if (--Lives <= 0) GameOver = true;
                }
                else
                {
                    if (moveResult == MoveResult.ReachedTarget) GameOver = true;
                    _playerPath.Add(newLocation);
                    Score++;
                }
            }

            return moveResult;
        }

        public BoardLocationState[,] GetGameData(bool showAll)
        {
            var boardData = Board.GameData;
            var fogOfWar = Board.FogOfWar;
            boardData[Board.PlayerLocation.X, Board.PlayerLocation.Y] = BoardLocationState.Player;

            Helpers.IterateBoard((x, y) =>
            {
                if (_playerPath.Any(p => p.X == x && p.Y == y) &&
                    !(Board.PlayerLocation.X == x && Board.PlayerLocation.Y == y))
                {
                    boardData[x, y] = BoardLocationState.PreviousPlayer;
                }
                else 
                {
                    if (fogOfWar[x, y] && !showAll)
                        boardData[x, y] = BoardLocationState.Unknown;
                }
            });
            return boardData;
        }
    }
}
