﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace MineBoardLogicLib
{
    public class Board
    {
        private readonly bool[,] _fogOfWar = new bool[8, 8];
        private readonly BoardLocationState[,] _states = new BoardLocationState[8, 8];
        public BoardLocation PlayerLocation { get; private set; }
        public BoardLocationState[,] GameData => (BoardLocationState[,])_states.Clone();    // To ensure the raw data can't be monkeyed with!
        public bool[,] FogOfWar => (bool[,])_fogOfWar.Clone();

        private readonly Random _rand = new Random();

        internal void SetTestPlayerLocation(int x, int y) => PlayerLocation = new BoardLocation {X = x, Y = y};
        internal void SetTestBoardLocationState(int x, int y, BoardLocationState state) => _states[x, y] = state;

        private List<BoardLocation> GenerateRandomSafePath()
        {
            var tempLocation = new BoardLocation { X = 1, Y = PlayerLocation.Y };   // First move is right one.
            var validPath = new List<BoardLocation> { PlayerLocation.Clone(), tempLocation.Clone() };

            void MoveUp()
            {
                var numMoves = _rand.Next(1, tempLocation.Y);
                while (numMoves-- > 0)
                {
                    tempLocation.Y--;
                    validPath.Add(tempLocation.Clone());
                }
            }

            void MoveDown()
            {
                var numMoves = _rand.Next(1, 7 - tempLocation.Y);
                while (numMoves-- > 0)
                {
                    tempLocation.Y++;
                    validPath.Add(tempLocation.Clone());
                }
            }

            while (tempLocation.X < 7)
            {
                // Choose up or down.
                if (tempLocation.Y == 0)
                    MoveDown();
                else if (tempLocation.Y == 7)
                    MoveUp();
                else
                {
                    if (_rand.Next(0, 2) == 0)
                        MoveUp();
                    else
                        MoveDown();
                }

                // Now move 1 right.
                tempLocation.X++;
                validPath.Add(tempLocation.Clone());
            }

            return validPath;
        }

        public void Init(int numMines)
        {
            // Set a random starting line that's not 0 or 7.
            // The player moves from the left to the right,
            PlayerLocation = new BoardLocation { X = 0, Y = _rand.Next(1, 7) };
            var validPath = GenerateRandomSafePath();
            Helpers.IterateBoard((x, y) =>
            {
                _fogOfWar[x, y] = true;
                _states[x, y] = BoardLocationState.Empty;
            });
            _fogOfWar[PlayerLocation.X, PlayerLocation.Y] = false;
            while (numMines > 0)
            {
                int x = _rand.Next(0, 7);
                int y = _rand.Next(0, 7);
                if (validPath.Any(p => p.X == x && p.Y == y)) continue;
                _states[x, y] = BoardLocationState.Mine;
                numMines--;
            }
        }

        public MoveResult DoMove(BoardLocation newLocation)
        {
            // Check boundary first.
            if (newLocation.X < 0 || newLocation.Y < 0 || newLocation.X > 7 || newLocation.Y > 7) return MoveResult.OutOfBounds;
            _fogOfWar[newLocation.X, newLocation.Y] = false;
            // If we are inside a Mine now...
            if (_states[newLocation.X, newLocation.Y] == BoardLocationState.Mine)
                return MoveResult.HitMine;
            PlayerLocation = newLocation;
            if (newLocation.X == 7) return MoveResult.ReachedTarget;

            return MoveResult.OK;
        }
    }
}
