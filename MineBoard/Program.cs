﻿using MineBoardLogicLib;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace MineBoard
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().Run(args);
            Console.WriteLine("Press a key to exit");
            Console.ReadKey();
        }

        Game _game;
        private string _lastMoveResult;
        private void Run(string[] args)
        {
            Console.WriteLine("MineBoard");
            Console.WriteLine("Choose a difficulty to start a new game, or Escape to exit.");
            char? chosen = ConsoleMenuLib.Menu.RunMenu(new Dictionary<char, string> {
                {'1', "Easy" },
                {'2', "Normal" },
                {'3', "Hard" },
                {'4', "Insane" },
            });
            if (chosen == null)
            {
                // End the game;
                Console.WriteLine("Thank you for playing.");
                return;
            }

            var gameDifficulty = chosen switch
            {
                '2' => GameDifficulty.Medium,
                '3' => GameDifficulty.Hard,
                '4' => GameDifficulty.Insane,
                _ => GameDifficulty.Easy
            };

            _game = new Game();
            _game.StartGame(gameDifficulty);

            GameLoop();
        }

        private void GameLoop()
        {
            while (!_game.GameOver)
            {
                DrawGame();
                var input = GetInput();
                if (input == null) break;
                HandleInput(input);
            }
            Console.Clear();
            DrawGame(true);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(_lastMoveResult);
            Console.WriteLine($"Game Over! Your score was: {_game.Score}");
        }

        private void HandleInput(char? input)
        {
            var moveType = input switch
            {
                'w' => MoveType.Up,
                'd' => MoveType.Right,
                's' => MoveType.Down,
                _ => MoveType.Left
            };
            var moveResult = _game.MakeMove(moveType);
            switch (moveResult)
            {
                case MoveResult.OutOfBounds:
                    _lastMoveResult = "Cannot move out of bounds";
                    break;
                case MoveResult.HitMine:
                    if (_game.GameOver)
                        _lastMoveResult = "You hit a mine and lost your last life!";
                    else _lastMoveResult = "You hit a mine and lost a life!";
                    break;
                case MoveResult.OK:
                    _lastMoveResult = "";
                    break;
                case MoveResult.ReachedTarget:
                    _lastMoveResult = "You reached the other side!";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private char? GetInput()
        {
            return ConsoleMenuLib.Menu.RunMenu(new Dictionary<char, string>
            {
                {'w', "Move Up"},
                {'d', "Move Right"},
                {'s', "Move Down"},
                {'a', "Move Left"},
            }, false);
        }

        private void DrawGame(bool showAll = false)
        {
            var gameData = _game.GetGameData(showAll);
            Console.Clear();

            Console.WriteLine($"Lives: {_game.Lives}.  Player Location: {_game.PlayerLocationAsChess}.  Score: {_game.Score}");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(_lastMoveResult);
            Console.ResetColor();
            Console.WriteLine();

            // <space> is empty
            // ? is unknown
            // O is current player
            // o is previous player path
            // X is Mine
            for (var y = 0; y < 8; y++)
            {
                var sb = new StringBuilder($"    {8 - y} ");
                for (var x = 0; x < 8; x++)
                {
                    var z = gameData[x, y] switch
                    {
                        BoardLocationState.Mine => 'X',
                        BoardLocationState.Player => 'O',
                        BoardLocationState.PreviousPlayer => 'o',
                        BoardLocationState.Empty => ' ',
                        _ => '-'
                    };
                    sb.Append($"{z} ");
                }
                Console.WriteLine(sb.ToString());
            }
            Console.WriteLine("      A B C D E F G H");
            Console.WriteLine();
        }
    }
}
