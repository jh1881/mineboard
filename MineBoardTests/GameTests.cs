﻿using System;
using System.Collections.Generic;
using System.Text;
using MineBoardLogicLib;
using NUnit.Framework;

namespace MineBoardTests
{
    class GameTests
    {
        [Test]
        public void MoveUp_Succeeds()
        {
            // Arrange
            var game = new Game();
            var board = game.Board;
            board.SetTestPlayerLocation(2, 2);

            // Act
            var moveResult = game.MakeMove(MoveType.Up);

            // Assert
            var location = board.PlayerLocation;
            Assert.AreEqual(MoveResult.OK, moveResult);
            Assert.AreEqual(1, location.Y);
            Assert.AreEqual(2, location.X);
        }

        [Test]
        public void MoveDown_Succeeds()
        {
            // Arrange
            var game = new Game();
            var board = game.Board;
            board.SetTestPlayerLocation(2, 2);

            // Act
            var moveResult = game.MakeMove(MoveType.Down);

            // Assert
            var location = board.PlayerLocation;
            Assert.AreEqual(MoveResult.OK, moveResult);
            Assert.AreEqual(3, location.Y);
            Assert.AreEqual(2, location.X);
        }

        [Test]
        public void MoveLeft_Succeeds()
        {
            // Arrange
            var game = new Game();
            var board = game.Board;
            board.SetTestPlayerLocation(2, 2);

            // Act
            var moveResult = game.MakeMove(MoveType.Left);

            // Assert
            var location = board.PlayerLocation;
            Assert.AreEqual(MoveResult.OK, moveResult);
            Assert.AreEqual(1, location.X);
            Assert.AreEqual(2, location.Y);
        }

        [Test]
        public void MoveRight_Succeeds()
        {
            // Arrange
            var game = new Game();
            var board = game.Board;
            board.SetTestPlayerLocation(2, 2);

            // Act
            var moveResult = game.MakeMove(MoveType.Right);

            // Assert
            var location = board.PlayerLocation;
            Assert.AreEqual(MoveResult.OK, moveResult);
            Assert.AreEqual(3, location.X);
            Assert.AreEqual(2, location.Y);
        }






        [Test]
        public void MoveUp_Returns_OutOfBounds()
        {
            // Arrange
            var game = new Game();
            var board = game.Board;
            board.SetTestPlayerLocation(0, 0);

            // Act
            var moveResult = game.MakeMove(MoveType.Up);

            // Assert
            var location = board.PlayerLocation;
            Assert.AreEqual(MoveResult.OutOfBounds, moveResult);
            Assert.AreEqual(0, location.X);
            Assert.AreEqual(0, location.Y);
        }

        [Test]
        public void MoveDown_Returns_OutOfBounds()
        {
            // Arrange
            var game = new Game();
            var board = game.Board;
            board.SetTestPlayerLocation(0, 7);

            // Act
            var moveResult = game.MakeMove(MoveType.Down);

            // Assert
            var location = board.PlayerLocation;
            Assert.AreEqual(MoveResult.OutOfBounds, moveResult);
            Assert.AreEqual(0, location.X);
            Assert.AreEqual(7, location.Y);
        }

        [Test]
        public void MoveLeft_Returns_OutOfBounds()
        {
            // Arrange
            var game = new Game();
            var board = game.Board;
            board.SetTestPlayerLocation(0, 2);

            // Act
            var moveResult = game.MakeMove(MoveType.Left);

            // Assert
            var location = board.PlayerLocation;
            Assert.AreEqual(MoveResult.OutOfBounds, moveResult);
            Assert.AreEqual(0, location.X);
            Assert.AreEqual(02, location.Y);
        }

        [Test]
        public void MoveRight_Returns_OutOfBounds()
        {
            // Arrange
            var game = new Game();
            var board = game.Board;
            board.SetTestPlayerLocation(7, 2);

            // Act
            var moveResult = game.MakeMove(MoveType.Right);

            // Assert
            var location = board.PlayerLocation;
            Assert.AreEqual(MoveResult.OutOfBounds, moveResult);
            Assert.AreEqual(7, location.X);
            Assert.AreEqual(2, location.Y);
        }




        [Test]
        public void MoveUp_Returns_HitMine()
        {
            // Arrange
            var game = new Game();
            var board = game.Board;
            board.SetTestPlayerLocation(2, 2);
            board.SetTestBoardLocationState(2,1,BoardLocationState.Mine);

            // Act
            var moveResult = game.MakeMove(MoveType.Up);

            // Assert
            var location = board.PlayerLocation;
            Assert.AreEqual(MoveResult.HitMine, moveResult);
            Assert.AreEqual(2, location.Y);
            Assert.AreEqual(2, location.X);
        }

        [Test]
        public void MoveHitsMine_Lives_ReducedByOne()
        {
            // Arrange
            var game = new Game();
            game.SetTestLives(3);
            var board = game.Board;
            board.SetTestPlayerLocation(2, 2);
            board.SetTestBoardLocationState(2, 1, BoardLocationState.Mine);

            // Act
            var moveResult = game.MakeMove(MoveType.Up);

            // Assert
            Assert.AreEqual(2, game.Lives);
        }

        [Test]
        public void MoveHitsMineWithOneLife_GameOver()
        {
            // Arrange
            var game = new Game();
            game.SetTestLives(1);
            var board = game.Board;
            board.SetTestPlayerLocation(2, 2);
            board.SetTestBoardLocationState(2, 1, BoardLocationState.Mine);

            // Act
            var moveResult = game.MakeMove(MoveType.Up);

            // Assert
            Assert.AreEqual(true, game.GameOver);
        }

        [Test]
        public void MoveReachesColumn7_Returns_ReachedTarget()
        {
            // Arrange
            var game = new Game();
            var board = game.Board;
            board.SetTestPlayerLocation(6, 2);

            // Act
            var moveResult = game.MakeMove(MoveType.Right);

            // Assert
            var location = board.PlayerLocation;
            Assert.AreEqual(MoveResult.ReachedTarget, moveResult);
        }
    }
}
