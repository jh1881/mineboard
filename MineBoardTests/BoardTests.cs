﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using MineBoardLogicLib;
using NUnit.Framework;

namespace MineBoardTests
{
    class BoardTests
    {
        [Test]
        public void Board_DoMove_Succeeds()
        {
            var board = new Board();
            board.SetTestPlayerLocation(2, 2);
            var newLocation = new BoardLocation { X = 2, Y = 3 };

            var moveResult = board.DoMove(newLocation);

            Assert.AreEqual(MoveResult.OK, moveResult);
            Assert.AreEqual(2, board.PlayerLocation.X);
            Assert.AreEqual(3, board.PlayerLocation.Y);
        }

        [Test]
        public void Board_DoMove_OnToMine_Returns_HitMine()
        {
            var board = new Board();
            board.SetTestPlayerLocation(2, 2);
            board.SetTestBoardLocationState(2, 3, BoardLocationState.Mine);
            var newLocation = new BoardLocation { X = 2, Y = 3 };

            var moveResult = board.DoMove(newLocation);

            Assert.AreEqual(MoveResult.HitMine, moveResult);
            Assert.AreEqual(2, board.PlayerLocation.X);
            Assert.AreEqual(2, board.PlayerLocation.Y);
        }

        [Test]
        public void Board_DoMove_OnToFinalColumn_Returns_ReachedTarget()
        {
            var board = new Board();
            board.SetTestPlayerLocation(6, 2);
            var newLocation = new BoardLocation { X = 7, Y = 2 };

            var moveResult = board.DoMove(newLocation);

            Assert.AreEqual(MoveResult.ReachedTarget, moveResult);
        }
    }
}
