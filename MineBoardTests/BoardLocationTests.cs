﻿using MineBoardLogicLib;
using NUnit.Framework;
using System;

namespace MineBoardTests
{
    class BoardLocationTests
    {
        [Test]
        public void BoardLocation_0_7_ToString_Gives_A1()
        {
            // Arrange
            var bl = new BoardLocation { X = 0, Y = 7 };
            var expected = "A1";

            // Act
            var s = bl.ToString();

            // Assert
            Assert.AreEqual(expected, s);
        }

        [Test]
        public void BoardLocation_7_0_ToString_Gives_H8()
        {
            // Arrange
            var bl = new BoardLocation { X = 7, Y = 0 };
            var expected = "H8";

            // Act
            var s = bl.ToString();

            // Assert
            Assert.AreEqual(expected, s);
        }

        [Test]
        public void BoardLocation_2_2_ToString_Gives_C6()
        {
            // Arrange
            var bl = new BoardLocation { X = 2, Y = 2 };
            var expected = "C6";

            // Act
            var s = bl.ToString();

            // Assert
            Assert.AreEqual(expected, s);
        }

        [Test]
        public void BoardLocation_0_8_Throws_Exception()
        {
            // Arrange, Act
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var b1 = new BoardLocation { X = 0, Y = 8 };
            });
        }
    }
}
